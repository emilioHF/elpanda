
public class ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.print("Introduce un numnero para saber si es primo: ");
		Scanner scan = new Scanner(System.in);
		int num = scan.nextInt();

		Boolean primo = esPrimo(num);

		if (!primo)
			System.out.println("\nEl numero " + num + " no es primo");
		else
			System.out.println("\nEl numero " + num + " es primo");

	}

	static Boolean esPrimo(int num) {
		Boolean primo = true;
		for (int a = 2; a < num; a++)
			if (num % a == 0)
				primo = false;

		return primo;

	}

}

